#include <stdio.h>


#define andcode(src, dest, len) and_code(src, dest, len, 1)
#define andecode(src, dest, len) and_code(src, dest, len, 0)


void and_code(unsigned char* src_pnt, unsigned char* dest_pnt, unsigned len, char encode){

	// keep this non-global
	/* sum of bits 7+8:192, 5+6:48, 3+4:12, 2+1:3 */
	static unsigned msb_val[]={192,48,12,3};

	register unsigned char bits_out=0, byte_out=0, bitpair_cnt=0;
	register unsigned char bit1, bit2, bits, byte;

	while(len--){

		byte=*src_pnt;

		//work on 4 pairs of bits at a time
		while(bitpair_cnt<4){

			// extract the two most significant bits from the byte stream
			bits = byte & (msb_val[bitpair_cnt]);
			// remove the msbits
			byte-=bits;
			// shift the msbits to the lsb
			bits= bits >> (8 - (bitpair_cnt+1) * 2);

			if(bits>1){
				bits -= 2;
				bit2 = 1;
			} else bit2 = 0;
			bit1=bits;

			if(encode){

				//symbolize that both bytes are equal	
				if(bit1==bit2){	
			// symbolize that when the bits match a value of 0, store the inverse in the second indicator
					bits_out=(!bit2)?3:2;
				}
				// when the bits dont match, bit1 has value of bit 2, swapped
				else if(bit2) bits_out=1;
			
			} else {

				// decode the src vector
				// if the bits match
				if(bit2){
					// both are 1, decode the inverse
					if(!bit1) bits_out=3;
				} else {
					// if the first bit is 1, second is zero
					// if the first bit is 0, second is one
					bits_out=(bit1)?2:1;
				} // the bits are not the same

			} // else decoding

			byte_out += bits_out;
			// remit miltiple declarations in the processing loop
			bits_out=0;

			// promote the lsb pairs
			if (bitpair_cnt<3) byte_out = byte_out << 2;

			//update the byte processing loop
			bitpair_cnt++;			

		} // while working on the bits
		// write out the encoded byte to the destination
		*dest_pnt=byte_out;
		// shift the input and output stream position
		dest_pnt++;
		src_pnt++;
		// clear the working vars
		bitpair_cnt=byte_out=0;

	} // while processing each byte

} // function




int main(){


unsigned char data[]={'1','2','3','4'};
unsigned char dest[4];

printf("%c",data[0]);
printf("%c",data[1]);
printf("%c",data[2]);
printf("%c",data[3]);
puts("\n");

andcode(data,dest,4);

/*
printf("%c\n",dest[0]);
printf("%c\n",dest[1]);
printf("%c\n",dest[2]);
printf("%c\n",dest[3]);
puts("\n");
*/

andecode(dest,data,4);

printf("%c",data[0]);
printf("%c",data[1]);
printf("%c",data[2]);
printf("%c",data[3]);
puts("\n");

return 0;
}


